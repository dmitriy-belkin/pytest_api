# GFC API Testing

[![pipeline status]https://gitlab.com/dmitriy-belkin/pytest_api/badges/main/pipeline.svg)](https://gitlab.com/dmitriy-belkin/pytest_api/-/commits/main)
<br>
<br>
![Python Version](https://img.shields.io/badge/Python-3.11.3-blue)
![Python Version](https://img.shields.io/badge/Requests-2.31.0-blue)
![Python Version](https://img.shields.io/badge/Pytest-7.3.1-blue)
## Packages
<table>
		<thead>
			<tr>
				<th><img width="96px" align="center" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Python_logo_and_wordmark.svg/2880px-Python_logo_and_wordmark.svg.png"> </th>
				<th><img width="69px" align="center" src="https://requests.readthedocs.io/en/latest/_static/requests-sidebar.png"></th>
				<th><img width="96px" align="center" src="https://545767148-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MdBdUMSCcMYTyNwZf80%2Fuploads%2Fgit-blob-f08a97a4a9cff017c204a21b66514ee07045dba8%2Fpytest.png?alt=media"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>3.11.3</td>
				<td>2.31.0</td>
				<td>7.3.1</td>
			</tr>
		</tbody>
	</table>

## `Reports`
<img width="96px" align="center" src="https://avatars.githubusercontent.com/u/5879127?s=200&v=4">
<br/>

Use Pages in Gitlab to view [Allure](https://pytest-api-dmitriy-belkin-e691073fe352fbc50137a81aacd0c8604c713.gitlab.io/).
> https://pytest-api-dmitriy-belkin-e691073fe352fbc50137a81aacd0c8604c713.gitlab.io/
 

## Setup instruction:
Clone repository.
```shell script
$ git clone https://gitlab.com/dmitriy-belkin/pytest_api.git
```
Add and activate new interpreter with Virtualenv Environment.
```shell script
$ python3 -m venv env
$ source env/bin/activate
```
Install all requirements from requirements.txt.
 ```bash script
$ pip install -r requirements.txt
