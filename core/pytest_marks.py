import pytest
import allure

# initialize drivers
init_driver = pytest.mark.usefixtures('init_drivers')

# allure report
title = allure.title
parent_suite = allure.parent_suite
suite = allure.suite
case_link = allure.link
severity = allure.severity
dynamic_title = allure.dynamic.title
dynamic_tag = allure.dynamic.tag
step = allure.step
description = allure.description

PROJECT_PREFIX = '0000'
TMS_URL = 'https://testit.ru/projects/0000/tests/{}'


def yandex_tracker_link(short_name):
    url = 'https://tracker.yandex.ru/agile/board/1'.format(short_name)
    return allure.issue(url, short_name)


# allure report links to test management system
def tms_link(case_number):
    return allure.testcase(TMS_URL.format(case_number), name='TestIT: ' + TMS_URL.format(case_number))


def dynamic_tms_link(case_number):
    return allure.dynamic.testcase(TMS_URL.format(case_number), name='TestIT: ' + TMS_URL.format(case_number))


skip = pytest.mark.skipif(True, reason='Временно не запускаем')
not_ready = pytest.mark.skipif(True, reason='Тест находится в разработке')
xfail = pytest.mark.xfail(strict=True)  # тест всегда падает (оформлен баг) #params: reason: str

parametrize = pytest.mark.parametrize  # параметризированные тесты

# pytest priority markers
blocker = pytest.mark.blocker  # Самый высокий приоритет (блокер)
critical = pytest.mark.critical  # Высокий приоритет (критичный)
normal = pytest.mark.normal  # Средний приоритет
minor = pytest.mark.minor  # Низкий приоритет

# pytest markers
smoke = pytest.mark.smoke
regression = pytest.mark.regression
