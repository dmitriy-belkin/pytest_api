import os
import requests
import logging

from fixtures.account import Account
from config import headers, _url_prod, _url_test, _url_temp, _delete_account
from core.pytest_marks import *

LOGGER = logging.getLogger(__name__)
account = Account().generate_data()


def pytest_addoption(parser):
    parser.addoption('--env', action='store', default='test', help='test и prod')
    parser.addoption('--flow', default='short', help='use short or full (full - with number confirmation)')
    parser.addoption('--execution_type', help='', default='local')


def pytest_configure(config):
    os.environ["env"] = config.getoption('env')
    os.environ["flow"] = config.getoption('flow')
    os.environ["execution_type"] = config.getoption('execution_type')


@allure.title('Выбор окружения для запуска теста: "{url}"')
@pytest.fixture(scope='session', autouse=True)
def url() -> str:
    """
    Парс окружение для запуска теста.
    """
    url = None
    execution_type = os.getenv("execution_type")
    if execution_type == 'local':
        env = os.getenv("env")
        if env == 'prod':
            url = f"{_url_prod}graphql/"
        elif env == 'test':
            url = f"{_url_test}graphql/"
        elif env == 'task':
            url = f"{_url_temp}graphql/"
        else:
            raise ValueError(f'--env="{env}" not found!')
    elif execution_type == 'gitlabci':
        env = os.getenv("env")
        if env == 'prod':
            url = f"{_url_prod}graphql/"
        elif env == 'test':
            url = f"{_url_test}graphql/"
        elif env == 'task':
            url = f"{_url_temp}graphql/"
        else:
            raise ValueError(f'--env="{env}" is wrong environment')
    logging.info(f"Your URL is {url}")
    return url


@allure.title('Создание и удаление аккаунта')
@pytest.fixture(scope="session", autouse=True)
def user_registration(url):
    """
    Создание тестового аккаунта перед началом сессии
    Удаление тестового аккаунта
    после выполнения всех тестовых сценариев
    """
    requests.get(url=f'{_url_test}{_delete_account}', headers=headers)
    env = os.getenv("env")

    gql = None
    registration = None
    max_attempts = 20
    attempts = 0

    while ((registration is None
           or (registration.json() and registration.json()["data"]["userRegistration"]["extra"] is None))
           and attempts < max_attempts):
        attempts += 1

        if env == 'test' or env == 'task':
            gql = f"""
                mutation UserRegistration {{
        userRegistration(
            input: {{
                source: SOURCE_SITE
                name: "{account.name}"
                secondName: "{account.secondName}"
                lastName: "{account.lastName}"
                phone: "{account.phone}"
                email: "{account.email}"
                password: "{account.password}"
                confirmConsent: true
                confirmPublicOffer: true
            }}
        ) {{
            extra {{
                code
                message
                debugInfo
            }}
        }}
    }}
        """
        elif env == 'prod':
            pass
        registration = requests.post(url=url, headers=headers, json={"query": gql})

        if registration.json()["data"]["userRegistration"]["extra"] is None:
            LOGGER.info(f"Регистрация прошла успешно! ", {account.lastName}, {account.name}, {account.secondName},
                        "Пароль: ", {account.password})
        elif registration.json()["data"]["userRegistration"] is not None:
            LOGGER.error('Пользователь уже создан или есть другая ошибка')
        elif registration.json()["data"]["userRegistration"]["extra"][0]["code"] == "USER_EMAIL_EXIST":
            LOGGER.error("Пользователь с такой почтой уже зарегистрирован")
            pytest.fail("Пользователь с такой почтой уже зарегистрирован")
        elif ["data"] not in registration.json():
            LOGGER.error(f'Не сработала ручка регистрации пользователя {registration.status_code}')
            pytest.fail(f'Не сработала ручка регистрации пользователя {registration.status_code}')
        else:
            raise ValueError(f"Invalid environment: {env}")

    yield

    if env == 'test':
        user_delete = requests.get(url=f'{_url_test}{_delete_account}',
                                   headers=headers)
        assert user_delete.json() == "success"
    elif env == 'prod':
        pass
    else:
        LOGGER.error('Ошибка в TearDown')


@allure.severity(allure.severity_level.CRITICAL)
@allure.title('Получение JSON Web Token (JWT) ')
@allure.description("""
Получение JSON Web Token.
Метод возвращает accessToken и refreshToken.
return tokens["accessToken"], tokens["refreshToken"]
""")
@pytest.fixture(scope='session', autouse=True)
def token(url, user_registration):
    """
    Получение JSON Web Token.
    Метод возвращает accessToken и refreshToken.
    return tokens["accessToken"], tokens["refreshToken"]
    """
    env = os.getenv("env")
    if env == 'test' or env == 'task':
        gql = f"""
           mutation {{
               userLogin(
                   login: "{account.email}"
                   password: "{account.password}"
               ) {{
                   tokens {{
                       accessToken
                       refreshToken
                   }}
               }}
           }}
           """
    else:
        raise ValueError(f"Invalid environment: {env}")

    result = requests.post(url=url, headers=headers, json={"query": gql})
    if result.status_code == 200 and result.json()["data"]["userLogin"]["tokens"] is not None:
        LOGGER.info("token generated")
    elif result.status_code == 200 and result.json()["data"]["userLogin"]["tokens"] is None:
        LOGGER.error(f"Token not generated. Code: {result.status_code}, but tokens is empty")
        pytest.fail(f"Статус код: {result.status_code} и токены не сгенерированы")
    else:
        LOGGER.error(f"Token not generated. Code: {result.status_code}")
        pytest.fail(f"Проблема с получением токена. Статус код: {result.status_code}")
    token = result.json()["data"]["userLogin"]["tokens"]["accessToken"]  # Only accessToken
    yield token


@allure.severity(allure.severity_level.CRITICAL)
@allure.title('Выбор города после авторизации')
@pytest.fixture(scope='session', autouse=True)
def change_city(url, header):
    gql = f"""
        mutation UserChangeCity {{
            userChangeCity(cityId: "{account.cityId}") {{
                extra {{
                    code
                    message
                    debugInfo
                }}
            }}
        }}
        """
    requests.post(url=url, headers=headers, json={"query": gql})
    yield


@allure.title('Header для запросов + JWT')
@pytest.fixture(scope="session", autouse=True)
def header(token):
    """
    Готовый Header.
    Используется accessToken
    """
    headers["Authorization"] = f"Bearer {token}"
    header = headers
    return header


@title("Подготовка аккаунта после регистрации")
@blocker
@pytest.fixture(scope="session", autouse=True)
def account_preparation(url, header):
    """
    1. Подтверждение номера телефона (для крона)
    2. Принятие оферты
    3. Добавление физ. лица
    """
    flow = os.getenv('flow')
    if flow == 'full':
        code = f"""
        mutation {{
            userPhoneSendConfirm(phone: "{account.phone}") {{
                extra {{
                    code
                    message
                    debugInfo
                }}
            }}
        }}
        """
        requests.post(url=url, headers=header, json={"query": code})

        confirmation = f"""
            mutation {{
                userPhoneCheckConfirm(phone: "{account.phone[1:]}", code: "{account.phone[-4:]}") {{
                    extra {{
                        code
                        message
                        debugInfo
                    }}
                }}
            }}
        """
        requests.post(url=url, headers=header, json={"query": confirmation})
    offer = """
        mutation ConfirmBonusProgram {
            userChange(input: {isBonusProgramConfirm: true}) {
                user {
              ...User
              __typename
            }
            extra {
              code
              message
              __typename
            }
            __typename
          }
        }

        fragment User on SiteTypeUserUserType {
          authorized {
            phone
            email
            priceCheaper {
              available
              notViewedCount
              __typename
            }
            needConfirmPhone
            needConfirmBonusProgram
            fullName
            havePurchasedProducts
            directionMenu {
              ...DirectionMenu
              __typename
            }
            organizations {
              dz
              pdz
              bonus
              haveSupplier
              supplierOfferConfirm
              list {
                ...Organization
                __typename
              }
              selectedSupplier {
                organizationId
                partnerId
                __typename
              }
              selected {
                organizationId
                partnerId
                __typename
              }
              __typename
            }
            __typename
          }
          city {
            ...City
            __typename
          }
          siteType
          experiments {
            code
            variant
            __typename
          }
          __typename
        }

        fragment Organization on SiteTypeOrganizationOrganizationType {
          id
          type
          name
          partners {
            ...Partner
            __typename
          }
          banks {
            ...Bank
            __typename
          }
          documents {
            type
            link
            name
            __typename
          }
          __typename
        }

        fragment Partner on SiteTypeOrganizationPartnerType {
          id
          name
          address {
            representation
            city
            __typename
          }
          documents {
            type
            name
            link
            __typename
          }
          deliveryTime {
            from
            to
            __typename
          }
          availableOperations
          availableTypes
          relationshipTypes
          needConfirmOffer
          __typename
        }

        fragment Bank on SiteTypeOrganizationBankType {
          paymentBill
          bankIdentificationCode
          correspondentBill
          bankName
          city
          swift
          bankId
          availableOperations
          __typename
        }

        fragment DirectionMenu on SiteTypeUserDirectionMenuType {
          id
          name
          __typename
        }

        fragment City on LocationCity {
          guid
          name
          phone
          code
          email
          catalogCityWarehouses {
            address
            schedule
            haveReplenishment
            __typename
          }
          chat {
            code
            config
            __typename
          }
          isContactOnly
          __typename
        }
                """
    requests.post(url=url, headers=header, json={"query": offer})

    add_organization = (
        """
        mutation organizationAdd {
            organizationAdd(
            individual: {
                partners: [
                    {
                        relationshipTypes: [restaurant]
                        nearCityGuid: "6361c458-7e05-11e5-9bb4-001e672286e8"
                        address: {
                        representation: "г Москва, поселение Внуковское, поселок дск Мичуринец, ул Ленина, д 23А"
                        city: "Москва"
                        district: null   
                        flat: null
                        house: "23А"
                        housing: null
                        locality: "дск Мичуринец"
                        postCode: 108816
                        region: "Москва"
                        street: "Ленина"
                    }
                }
            ]
        }
    ) {
    ids
    extra {
      code
      message
      debugInfo
    }
  }
}
        """
    )
    requests.post(url=url, headers=header, json={"query": add_organization})
    # if result.json()["data"]["userPhoneCheckConfirm"]["extra"][0]["code"] == "CODE_EXPIRED":
    #     LOGGER.error(f'Код не действителен: {result.json()["data"]["userPhoneCheckConfirm"]["extra"][0]["code"]}')
    yield


@allure.title('Фикстура для очищения тестовых данных')
@pytest.fixture(scope="function", autouse=True)
def cleaner(url, header):
    """
    SetUp & TearDown
    """
    # Проверка корзины перед выполнением теста
    cart_check = (
        """
        {
    basket {
      totalPrice
      items {
        priceTotal
        quantityMax
        quantityTotal
      }
        successMessage
        errorMessage
        applied
    }
}
        """
    )
    cart = requests.post(url=url, headers=header, json={"query": cart_check})

    if cart.json()["data"]["basket"]["items"] != 0:
        # Очищаем корзину, если она не пустая
        cart_clear = (
            """
            mutation {basketClear}
            """
        )
        requests.post(url=url, headers=header, json={"query": cart_clear})
    yield
    # Проверка корзины после выполнения теста
    cart_check = (
        """
        {
    basket {
      totalPrice
      items {
        priceTotal
        quantityMax
        quantityTotal
      }
        successMessage
        errorMessage
        applied
    }
}
        """
    )
    cart = requests.post(url=url, headers=header, json={"query": cart_check})

    if cart.json()["data"]["basket"]["items"] != 0:
        # Очищаем корзину, если она не пустая
        cart_clear = (
            """
            mutation {basketClear}
            """
        )
        requests.post(url=url, headers=header, json={"query": cart_clear})
