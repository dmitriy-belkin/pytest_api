import os

# Все для заголовков
gql_ver = {'x-gql-version': os.environ.get('GQL_V')}
test_ver = {'x-app-version': os.environ.get('APP_V')}
api_key = {'api-key': os.environ.get('API_KEY')}
secret_key = {'secret': os.environ.get('SECRET_KEY')}

# Заголовок для запросов (не авторизованный пользователь)
headers = {**gql_ver, **test_ver, **api_key, **secret_key}
_no_auth_header = {**gql_ver, **test_ver, **api_key, **secret_key}

# URL на окружения
_url_test = os.environ.get('TEST')
_url_prod = os.environ.get('PROD')
_url_temp = os.environ.get('DEV')

# Основной город
_main_city = os.environ.get('CITY')

# Удаление тестового пользователя
_delete_account = "api/v3/test/teardown/?apiKey=" + str(os.environ.get('DELETE_KEY'))

# ЗАКАЗ
# Комментарий к заказу
_comment = os.environ.get('COMMENT')
