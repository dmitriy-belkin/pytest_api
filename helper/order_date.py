from datetime import datetime, timedelta


def date():
    current_datetime = datetime.now()
    days_to_add = 2
    added_days = 0
    while added_days < days_to_add:
        current_datetime += timedelta(days=1)
        if current_datetime.weekday() < 5:
            added_days += 1
    unix_timestamp = current_datetime.timestamp()
    order_date = int(unix_timestamp)
    return order_date
