# import logging
# import requests
#
# from config import headers
# from core.pytest_marks import *
#
# LOGGER = logging.getLogger(__name__)
#
#
# @pytest.fixture(scope="session")
# def cities_list(url, header):
#     """
#     Отображение всех городов
#     """
#     gql = """
#     query CitiesList {locationCities {guid name}}
#     """
#
#     cities = requests.post(url=url, headers=headers, json={"query": gql})
#     cities_data = cities.json()
#     cities_dict = {city["name"]: city["guid"] for city in cities_data["data"]["locationCities"]}
#     return cities_dict
#
#
# def city(url):
#     # Получаем словарь с названиями городов и их GUID
#     cities_data = cities_list("Москва", cities_list(url=url, headers=headers))
#
#     # Используем словарь для получения guid города по его названию
#     city_name = "Москва"
#     city_guid = cities_data.get(city_name)
#     if city_guid:
#         print(city_guid)  # Выведет guid города Москва
#     else:
#         print(f"Город с названием '{city_name}' не найден")