import random
import string
import os
from russian_names import RussianNames
from dataclasses import dataclass


@dataclass
class Account:
    source: str = ""
    name: str = ""
    secondName: str = ""
    lastName: str = ""
    cityId: str = ""
    phone: str = ""
    email: str = ""
    # confirmConsent: bool = ""
    # confirmPublicOffer: bool = ""
    password: str = ""

    def __post_init__(self):
        """Вызывается после инициализации экземпляра класса"""
        if not self.password:
            self.password = self.password_generator(8)

    @staticmethod
    def password_generator(length):
        """
        Генерация пароля согласно требованиям:
            mb_strlen($password) < 6
            || !preg_match('/[A-ZА-Я]/u', $password)
            || !preg_match('/[a-zа-я]/u', $password)
            || !preg_match('/[0-9]/', $password)

        1. Генерация обязательных символов для пароля
        2. Хотя бы одна цифра
        3. Хотя бы одна буква в верхнем регистре
        4. Хотя бы один символ из пунктуации
        5. Хотя бы одна буква в нижнем регистре
        6. Генерация оставшейся части пароля
        7. Перемешивание пароля для дополнительной случайности
        """
        characters = string.ascii_uppercase + string.ascii_lowercase + string.digits + string.punctuation
        password = random.choice(string.digits)
        password += random.choice(string.ascii_uppercase)
        password += random.choice(string.punctuation)
        password += random.choice(string.ascii_lowercase)
        password += ''.join(random.sample(characters, length - len(password)))
        password = ''.join(random.sample(password, len(password)))
        return password

    @classmethod
    def generate_data(cls):
        """Формирование данных для создания аккаунта"""

        russian_names = RussianNames(
            count=1, patronymic=True, transliterate=False, output_type='dict').get_person()
        return cls(
            source="SOURCE_SITE",
            name=russian_names["name"],
            secondName=russian_names["patronymic"],
            lastName=russian_names["surname"],
            cityId=os.environ.get('CITY_ID'),
            phone=os.environ.get('PHONE'),
            email=os.environ.get('EMAIL'),
            # confirmConsent='true',
            # confirmPublicOffer='true',
        )

    # @staticmethod
    # def account_data(generate_data):
    #     """Преобразование данных пользователя в JSON"""
    #     account_dict = asdict(generate_data)
    #     account = json.dumps(account_dict, indent=2, ensure_ascii=False)
    #     print(account)
    #     return account
