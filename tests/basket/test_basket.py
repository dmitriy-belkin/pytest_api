import requests
import logging
from core.pytest_marks import *

LOGGER = logging.getLogger(__name__)


@parent_suite('Корзина')
@suite("Состояние корзины")
@title('Проверка корзины')
@tms_link('2177')
@severity('critical')
@critical
def test_basket(header, url):
    """
    Проверяем сумму корзины и товары.
    """
    gql = (
        """
mutation {
  basketProductUpsert(
    guid: "df486337-b708-11e2-84bb-50e549559438"
    quantity: 12
  ) {
    basket {
      totalPrice
      items {
        product{
          name
          guid
          offer{
            amount
          }
        }
        priceTotal
        quantityMax
        quantityTotal
        quantityOffers {
          isReplenishment
          quantity
        }
        canBuy
      }
    }
    extra {
      code
      message
      debugInfo
    }
  }
}
        """
    )
    response = requests.post(
        url=url,
        headers=header,
        json={"query": gql})
    if response.status_code == 200:
        assert response.json()["data"]["basketProductUpsert"]["basket"] is not None
        assert response.json()["data"]["basketProductUpsert"]["basket"]["items"][0]['product']['guid'] == "df486337-b708-11e2-84bb-50e549559438"
        assert response.json()["data"]["basketProductUpsert"]["basket"]["items"][0]['quantityTotal'] == 12
        assert response.json()["data"]["basketProductUpsert"]["basket"]['items'][0]['product']['name'] == "Рис Shinaki 25кг, Россия"
        LOGGER.info("Корзина очищена")
    else:
        LOGGER.error(f"Status Code: {response.status_code}")
        pytest.fail(f"Status Code: {response.status_code}")
