import logging
import requests

from core.pytest_marks import *

LOGGER = logging.getLogger(__name__)


@parent_suite("Корзина")
@suite("Добавление товара в корзину")
@title("Добавление товара в корзину")
@tms_link('2012')
@description("""
guid: '0139afcf-431e-11ed-85b6-00155d630b00',
Товар добавленный в корзину: "Молоко кокосовое SUREE INTERFOODS CO 1л, 12шт/кор"
""")
@severity('blocker')
@blocker
def test_marketplace_add_to_cart(url, header):
    """
    Добавление товара в корзину
    guid: '0139afcf-431e-11ed-85b6-00155d630b00',
    Товар добавленный в корзину: "Молоко кокосовое SUREE INTERFOODS CO 1л, 12шт/кор"
    """
    gql = (
        """
    mutation {
  basketProductUpsert(guid: "0139afcf-431e-11ed-85b6-00155d630b00" , quantity: 30) {
  basket {
    totalPrice
    totalPriceOld  
    items {
        product {
            name
            }
        }
    }
  }
}
"""
    )
    response = requests.post(
        url=url,
        headers=header,
        json={"query": gql})
    assert response.status_code == 200
    assert response.json()["data"]["basketProductUpsert"]["basket"]['items'][0]['product'][
               'name'] == "Молоко кокосовое SUREE INTERFOODS CO 1л, 12шт/кор"
