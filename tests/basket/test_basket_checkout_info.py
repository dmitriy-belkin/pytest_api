import requests
import logging
from core.pytest_marks import *

LOGGER = logging.getLogger(__name__)


@parent_suite('Корзина')
@suite("Корзина")
@title('Информация о корзине')
@tms_link('2185')
@severity('critical')
@critical
def test_basket_checkout_info(header, url):
    """
    Получаем информацию:
    О составе корзины
    Возможные дни доставки
    Возможные дни самовывоза
    Все информационные сообщения
    Склад отгрузки
    данные необходимые для оформления заказа
    """
    with allure.step("Проверка информации о пустой корзине"):
        gql = (
            """
    query BasketCheckoutInfo {
        basketCheckoutInfo {
            checkoutInfo {
                canCheckout
                maxBonusCanPay
                productBonusTotal
                messages {
                    code
                    type
                    message
                }
                deliveries {
                    type
                    name
                    forBasketType
                    availableDeliveryDays
                    minPrice
                    pickupAddress
                }
                subBaskets {
                    name
                    type
                    temperatureCondition
                    totalPrice
                    totalPriceOld
                    totalWeightGross
                }
                payments {
                    type
                    name
                }
            }
            extra {
                code
                message
                debugInfo
            }
        }
    }
            """
        )
        response = requests.post(
            url=url,
            headers=header,
            json={"query": gql})
        if response.status_code == 200:
            assert response.json()["data"]["basketCheckoutInfo"]["checkoutInfo"] is None, 'В корзине нет товаров'
        else:
            LOGGER.error(f"Status Code: {response.status_code}")
            pytest.fail(f"Status Code: {response.status_code}")

    with allure.step("Добавление товара в корзину"):
        gql = (
            """
        mutation {
      basketProductUpsert(guid: "0139afcf-431e-11ed-85b6-00155d630b00" , quantity: 30) {
      basket {
        totalPrice
        totalPriceOld  
        items {
            product {
                name
                }
            }
        }
      }
    }
    """
        )
        response = requests.post(
            url=url,
            headers=header,
            json={"query": gql})
        assert response.status_code == 200
        assert response.json()["data"]["basketProductUpsert"]["basket"]['items'][0]['product'][
                   'name'] == "Молоко кокосовое SUREE INTERFOODS CO 1л, 12шт/кор"

    with allure.step("Получение информации о корзине с товаром"):
        gql = (
            """
    query BasketCheckoutInfo {
        basketCheckoutInfo {
            checkoutInfo {
                canCheckout
                maxBonusCanPay
                productBonusTotal
                messages {
                    code
                    type
                    message
                }
                deliveries {
                    type
                    name
                    forBasketType
                    availableDeliveryDays
                    minPrice
                    pickupAddress
                }
                subBaskets {
                    name
                    type
                    temperatureCondition
                    totalPrice
                    totalPriceOld
                    totalWeightGross
                }
                payments {
                    type
                    name
                }
            }
            extra {
                code
                message
                debugInfo
            }
        }
    }
            """
        )
        response = requests.post(
            url=url,
            headers=header,
            json={"query": gql})
        if response.status_code == 200:
            assert response.json()["data"]["basketCheckoutInfo"]["checkoutInfo"]["deliveries"][0] is not None
            assert response.json()["data"]["basketCheckoutInfo"]["checkoutInfo"]["deliveries"][1] is not None
            assert response.json()["data"]["basketCheckoutInfo"]["checkoutInfo"]["payments"][0]["name"] == "наличными"
            assert response.json()["data"]["basketCheckoutInfo"]["checkoutInfo"]["payments"][1]["name"] == ("картой "
                                                                                                            "курьеру")
        else:
            LOGGER.error(f"Status Code: {response.status_code}")
            pytest.fail(f"Status Code: {response.status_code}")
