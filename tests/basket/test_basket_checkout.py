import requests
import logging

from core.pytest_marks import *
from helper.order_date import date
from fixtures.account import Account
from config import _comment

LOGGER = logging.getLogger(__name__)


@parent_suite('Корзина')
@suite("Чекаут")
@title('Оформление заказа (чекаут)')
@tms_link('2185')
@severity('critical')
@critical
def test_basket_checkout(header, url):
    """
    Чекаут
    """
    account = Account().generate_data()
    with allure.step('Чекаут пустой корзины, склад'):
        gql = (
            f"""
    mutation BasketCheckout($unixTimeStamp: Int!) {{
        basketCheckout(
            platformSource: site
            phone: "{account.phone}"
            payBonus: 0
            delivery: {{type: COURIER, forBasketType: "customerWarehouse", date: $unixTimeStamp}}
            paymentType: CASH
            needCallFromManager: false
            bankId: null
            comment: "{_comment}"
            fullName: {{name: "{account.name}", secondName: "{account.secondName}" lastName: "{account.lastName}"}}
        ) {{
            numbers 
            extra {{
                code
                message
                debugInfo
            }}
        }}
    }}
            """
        )
        response = requests.post(
            url=url,
            headers=header,
            json={"query": gql, "variables": {"unixTimeStamp": date()}})
        assert response.json()['data']['basketCheckout']['extra'][0]['message'] == 'В корзине нет товаров'
        assert response.json()['data']['basketCheckout']['extra'][0]['code'] == 'BASKET_EMPTY'

    with allure.step('Чекаут пустой корзины, доставка'):
        gql = (
            f"""
            mutation BasketCheckout($unixTimeStamp: Int!) {{
                basketCheckout(
                    platformSource: site
                    phone: "{account.phone}"
                    payBonus: 0
                    delivery: {{type: COURIER, forBasketType: "replenishment", date: $unixTimeStamp}}
                    paymentType: CASH
                    needCallFromManager: false
                    bankId: null
                    comment: "{_comment}"
                    fullName: {{name: "{account.name}", secondName: "{account.secondName}", 
                    lastName: "{account.lastName}"}}
                ) {{
                    numbers 
                    extra {{
                        code
                        message
                        debugInfo
                    }}
                }}
            }}
                    """
        )
        response = requests.post(
            url=url,
            headers=header,
            json={"query": gql, "variables": {"unixTimeStamp": date()}})
        assert response.json()['data']['basketCheckout']['extra'][0]['message'] == 'В корзине нет товаров'
        assert response.json()['data']['basketCheckout']['extra'][0]['code'] == 'BASKET_EMPTY'