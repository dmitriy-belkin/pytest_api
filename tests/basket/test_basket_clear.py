import requests
import logging
from core.pytest_marks import *

LOGGER = logging.getLogger(__name__)


@parent_suite('Корзина')
@suite("Корзина")
@title('Очистка корзины')
@tms_link('2364')
@severity('critical')
@critical
def test_basket_clear(header, url):
    """
    Очистка корзины
    """
    gql = (
        """
mutation {
    basketClear {
        basket {
            totalPrice
            totalDiscountPrice
            totalWeight
            totalBonus
            products {
                offerId
                image
                name
                link
                price
                bonus
                discountPrice
                totalPrice
                totalBonus
                potential
                quantity
                maxQuantity
                measureRatio
                measureSymbol
                brandName
                section {
                    name
                    menuIcon
                }
                canBuy
                haveAnalogs

            }
        }
        extra {
            code
            message
        }
    }
}
        """
    )
    response = requests.post(
        url=url,
        headers=header,
        json={"query": gql})
    if response.status_code == 200 and response.json()["data"]["basketClear"]["basket"]["totalPrice"] == 0:
        assert response.json()["data"]["basketClear"]["basket"]["totalPrice"] == 0
        LOGGER.info("Корзина очищена")
    elif response.status_code == 200 and response.json()["data"]["basketClear"]["basket"]["totalPrice"] != 0:
        LOGGER.info("Корзина не пустая!")
    else:
        LOGGER.error(f"Status Code: {response.status_code}")
        pytest.fail(f"Status Code: {response.status_code}")
